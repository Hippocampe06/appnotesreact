import React, { useState, useEffect } from 'react';

import { useNavigate } from "react-router-dom";
    

const useApp = () => {

    const [inputValue, setInputValue] = useState<string>("");
    const [todos, setTodos] = useState<Array<string>>([]);
    const [id, setId] = useState<number>(0);

    const navigate = useNavigate();

    const handleChange = (event: any) => {
        setInputValue(event.target.value);
    }

    const onSubmit = () => {
        const newTodos = Array.from(todos.concat(inputValue));
        localStorage.setItem("todos", JSON.stringify(newTodos));

        setInputValue("");
        setTodos(newTodos);

        navigate('/');
    }

    useEffect(() => {
        let todosCached = localStorage.getItem("todos");
        if (todosCached) {
            setTodos(JSON.parse(todosCached));
        } else {
            localStorage.setItem("todos", "[]");
            setTodos([]);
        }
    }, []);

    return {
        todos,
        inputValue,
        handleChange,
        setInputValue,
        onSubmit,
        setId,
        id
    }

}

export default useApp;
