export { default as Home } from "./Home";
export { default as Header } from "./Header";
export { default as Create } from "./Create";
export { default as Detail } from "./Detail";