import React, { useEffect, useState } from "react";

type DetailProps = {
    id: number
}

export default function Detail(props: DetailProps) {

    const [todo, setTodo] = useState("");

    useEffect(() => {
        console.log("props.id", props.id);
        let todos = localStorage.getItem("todos");
        if (todos) {
            let currentTodo = JSON.parse(todos)[props.id];
            setTodo(currentTodo);
        } else {
            setTodo("");
        }
    }, []);

    return(
        <p>{todo}</p>
    )
}