import React from 'react';

import { useNavigate } from "react-router-dom";

type HomeProps = {
    todos: string[],
    setId: (e: number) => void
}

export default function Home(props: HomeProps) {
    const { todos, setId } = props;
    const navigate = useNavigate();
    
    const goToDetail = (id: number) => {
        console.log("id", id);
        setId(id);
        navigate('/detail');
    }
    
    return(
        <div className="notes">
            {
                todos.map((todo, index) => <div onClick={() => goToDetail(index)} key={index}>{todo}</div>)
            }
        </div>
    )
}