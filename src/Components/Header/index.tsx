import React from 'react';

import { Link } from "react-router-dom";

import logo from "../../logo.png";
import plus from "../../plus.png";

export default function Header() {
    return (
        <header className="App-header">
            <Link to="/">
                <img src={logo} className="headerImg"/>
            </Link>
            <Link to="/create">
                <img src={plus} className="headerImg"/>
            </Link>
        </header>
    )
}