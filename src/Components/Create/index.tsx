import React from "react";



type createProps = {
    inputValue: string,
    handleChange: (event: any) => void,
    onSubmit: () => void
}

export default function Create(props: createProps) {
    const { inputValue, handleChange, onSubmit } = props;
    
    return(
        <React.Fragment>
            <div>
                <input
                    className="todoInput" 
                    value={inputValue}
                    onChange={handleChange} />

                <button onClick={onSubmit}>Submit</button>

                
            </div>
            <div>
                Hello world
            </div>
        </React.Fragment>
    )
}