import React from 'react';
import './App.scss';

import useApp from './Hooks/useApp';

import { Home, Header, Create, Detail } from "./Components";

import {
	Routes,
	Route
} from "react-router-dom";

function App() {
	const { inputValue, onSubmit, handleChange, todos, setId, id } = useApp();
	return (
		<div className="App">
			<Header />
			<Routes>
				<Route path="/" element={<Home todos={todos} setId={setId} />} />
				<Route 
					path="/create"
					element={
						<Create 
							inputValue={inputValue} 
							onSubmit={onSubmit} 
							handleChange={handleChange} 
						/>
					}
				/>
				<Route path="/detail" element={<Detail id={id} />} />
			</Routes>
		</div>
	);
}

export default App;
